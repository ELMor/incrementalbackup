package es.olmo.bi;


public class ProgressShower {
	
	String spaces="          ";
	String sameLineList=(System.getProperty("multipleListLog")!=null)?"\n":"";
	long problemSize;
	int lineSize;
	String staticMsg;
	long currentValue=0;
	long initTime=-1;
	
	public ProgressShower(int lineSz,long sz, String msg){
		lineSize=lineSz;
		while(spaces.length()<lineSize)
			spaces+=spaces;
		spaces=spaces.substring(0,lineSize);
		problemSize=sz;
		staticMsg=msg;
		initTime=System.currentTimeMillis();
	}
	public ProgressShower(int lineSz,long sz, String msg, long avanceInicial){
		this(lineSz,sz,msg);
		currentValue=avanceInicial;
	}
	
	public void report(long avance,String msg){
		currentValue+=avance;
		System.out.print(leftMsg(msg));
	}

	public void report(int avance, String msg, String rightMsg){
		currentValue+=avance;
		String left=leftMsg(msg);
		while(rightMsg.length()>left.length())
			rightMsg=rightMsg.substring(2);
		left=left.substring(0,lineSize-rightMsg.length())+rightMsg;
		System.out.print(left);
	}

	private String leftMsg(String msg) {
		// time now
		long now=System.currentTimeMillis();
		// interval transcurred til now
		long interval = now - initTime ;
		// speed of process [units per milisecond]
		float speed= currentValue / (interval != 0 ? interval :1 );
		// estimate total time at current speed [miliseconds]
		float totalTime= problemSize / ( speed != 0 ? speed : 1 ) ;
		// ETA Estimated time abend (milis)
		long eta = (long)totalTime - interval + 900;
		long horas= (eta/1000) / 3600;
		long minutos = ((eta/1000) - (horas*3600)) / 60 ;
		long segundos = (eta/1000) - horas*3600 - minutos * 60 ;
		long pc=((currentValue)*100)/problemSize;
		String show=String.format("\r%02d%% ETA %d:%02d:%02d "+staticMsg+" "+msg, pc,horas,minutos,segundos);
		while(show.length()<lineSize)
			show+=spaces;
		return show.substring(0,lineSize);
	}
	
	public long  getCurrentValue(){
		return currentValue;
	}
}
